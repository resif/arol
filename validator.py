#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import yaml
import jsonschema
import jsonref
import os
import logging
import itertools

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('validator')


def read_schema(schema_path):
    with open(schema_path, 'r') as f:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        base_uri = f'file://{current_dir}/schema/'
        schema = jsonref.loads(f.read(), base_uri=base_uri, jsonschema=True)
    return schema


def get_pz_format(file_name):
    return file_name.split('/')[-1].split('.')[-2]


def read_key_schema():
    return read_schema(f'schema/key.schema.json')


def read_pz_schema(pz_format):
    return read_schema(f'schema/{pz_format}.schema.json')


def has_errors():
    return 40 in logger._cache and logger._cache[40]


def get_navi_file_path(path, sub_dirs):
    return os.path.join(path, sub_dirs, f'{sub_dirs}.yaml')


def load_yaml_file(path):
    with open(path, 'r') as stream:
        return yaml.safe_load(stream)


def validate_navi_file(path, sub_dirs):
    navi_file = get_navi_file_path(path, sub_dirs)
    if not os.path.isfile(navi_file):
        logger.warning(f'Cannot find the following navigation file: {navi_file}')
        return
    schema = read_key_schema()
    valid_schema = jsonschema.Draft4Validator(schema)
    instance = yaml.safe_load(open(navi_file, 'r'))
    if not valid_schema.is_valid(instance):
        for error in valid_schema.iter_errors(instance):
            error_element = "".join(f"[{err}]" for err in error.path)
            logger.error(f"{navi_file}: {error_element}: {error.message}")
        return
    declared_filters = set(map(lambda x: x['code'], instance['filters']))

    undefined_filters = set(instance['mandatory_filters']) - declared_filters
    if len(undefined_filters) > 0:
        logger.error(f"{navi_file}: mandatory filter contains undefined filter: {undefined_filters}")

    used_filters = []
    applicable_filters = {}
    for response_file in instance['responses']:
        response_path = os.path.join(path, response_file['path'])
        if not os.path.isfile(response_path):
            logger.error(f'Cannot find {response_path} file defined in {navi_file} file')
        used_filters.extend(list(response_file['applicable_filters'].keys()))
        applicable_filters[response_file['path']] = str(dict(sorted(response_file['applicable_filters'].items())))
    used_filters = set(used_filters)
    unused_filters = declared_filters - used_filters
    if len(unused_filters) > 0:
        logger.error(f'The {",".join(unused_filters)} filter(s) are declared but not used. Key file: {navi_file}')
    undeclared_filters = used_filters - declared_filters
    if len(undeclared_filters) > 0:
        logger.error(f'The {",".join(undeclared_filters)} filter(s) are used but not declared. Key file: {navi_file}')
    duplicates = {}
    for i, v in applicable_filters.items():
        duplicates[v] = [i] if v not in duplicates.keys() else duplicates[v] + [i]
    for key in duplicates:
        if len(duplicates[key]) > 1:
            logger.error(f'The following filters have identical declarations: {", ".join(duplicates[key])}')


def validate_response_files(path, sub_dirs):
    pz_formats = ['response', 'filter']
    full_path = os.path.join(path, sub_dirs)
    file_names = os.listdir(full_path)
    file_names = list(filter(lambda x: x != f'{sub_dirs}.yaml', file_names))
    file_names = list(filter(lambda x: x != 'include', file_names))
    for file_name in file_names:
        file_path = os.path.join(full_path, file_name)
        pz_format = get_pz_format(file_name)
        if pz_format not in pz_formats:
            logger.error(f'The following file has a wrong name(must be: *.response.yaml or *.filter.yaml): {file_path}')
            continue
        schema = read_pz_schema(pz_format)
        valid_schema = jsonschema.Draft4Validator(schema)
        instance = yaml.safe_load(open(file_path, 'r'))
        for error in valid_schema.iter_errors(instance):
            error_element = "".join(f"[{err}]" for err in error.path)
            logger.error(f"{file_path}: {error_element}: {error.message}")


def get_navigation_options(directory, sub_dirs):
    navi_file_instance = load_yaml_file(get_navi_file_path(directory, sub_dirs))
    options = []
    for declared_filter in navi_file_instance['filters']:
        declared_filter_code = declared_filter['code']
        option = set()
        for response in navi_file_instance['responses']:
            applicable_filters = response['applicable_filters']
            if declared_filter_code in applicable_filters:
                option.add((declared_filter_code, applicable_filters[declared_filter_code]))
        options.append(list(option))
    result = {}
    for selections in itertools.product(*options):
        result[selections] = []
        for response in navi_file_instance['responses']:
            available_options = [(k, v) for k, v in response['applicable_filters'].items()]
            unmatched_options = set(available_options) - set(selections)
            if len(unmatched_options) == 0:
                result[selections].append(response['path'])

    return result


def validate_navigation_datalogger(directory):
    for sub_dirs in os.listdir(directory):
        options = get_navigation_options(directory, sub_dirs)
        for filter_options, result in options.items():
            if len(result) > 2:
                logger.error(f"Datalogger filter provides {len(result)} response file(s) (must be 1 or 2)."
                             f" Filter applied: {filter_options}; Result: {result}")
            elif len(result) > 0:
                ad_conversions = 0
                for res in result:
                    response = load_yaml_file(os.path.join(directory, res))
                    stages = response['response']['stages']
                    conversions = filter(lambda x: x.get('filter', {'type': ''}).get('type') == 'AD_CONVERSION', stages)
                    if len(list(conversions)) > 0:
                        ad_conversions += 1
                if ad_conversions != 1:
                    logger.warning(f"There must be 1 response (not {ad_conversions}) "
                                   f"file with 'AD_CONVERSION' filter type."
                                   f" Datalogger filter applied: {filter_options}; Result: {result}")


def validate_navigation_sensor(directory):
    for sub_dirs in os.listdir(directory):
        options = get_navigation_options(directory, sub_dirs)
        for filter_options, result in options.items():
            if len(result) > 1:
                logger.error(f"Sensor filter provides {len(result)} response file(s) (must be 1)."
                             f" Filter applied: {filter_options}; Result: {result}")


def validate(directory):
    for sub_dirs in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, sub_dirs)):
            logger.error(f'The following file should be in a directory: {os.path.join(directory, sub_dirs)}')
            continue
        validate_navi_file(directory, sub_dirs)
        validate_response_files(directory, sub_dirs)


def exec_validator_details(file_path, schema_instance):
    valid_schema = jsonschema.Draft4Validator(schema_instance)
    instance = yaml.safe_load(open(file_path, 'r'))
    for error in valid_schema.iter_errors(instance):
        error_element = "".join(f"[{err}]" for err in error.path)
        logger.error(f"{error_element}: {error.message}")


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(prog='AROL validator', description=__doc__)
    group = parser.add_argument_group()
    group.add_argument("-k", "--keyfile", help='Path to navigation file')
    group.add_argument("-r", "--respfile", help='Path to response file')
    args = parser.parse_args()

    if args.keyfile:
        exec_validator_details(args.keyfile, read_key_schema())
    elif args.respfile:
        exec_validator_details(args.respfile, read_pz_schema(get_pz_format(os.path.basename(args.respfile))))
    else:
        datalogger = 'objects/Dataloggers'
        sensor = 'objects/Sensors'
        validate(datalogger)
        validate(sensor)
        if not has_errors():
            validate_navigation_datalogger(datalogger)
            validate_navigation_sensor(sensor)
        sys.exit(1) if has_errors() else print('All files are correct!')
