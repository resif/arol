# AROL validator

## Run validator locally
1. run `pip install -r requirements.txt` to install required Python libraries
2. run `python validator.py` to validate entire AROL. The validator will check `objects/Dataloggers` and `objects/Sensors` folders

## Validation rules
* Schema validation. Schema files are located in `/schema` folder
* Existence of navigation file. Each folder should have `{FOLDER_NAME}.yaml` file
* Declared and used filters in a navigation file should be consistent
* There should not be identical filter definitions in one navigation file
* Response file name conventions: *.response.yaml or *.filter.yaml
* Any final datalogger filter combinations should provide 1 or 2 response files
* Any final datalogger filter combinations should provide one response file which has filter_type = AD_CONVERSION
* Any final sensor filter combinations should provide 1 response file



