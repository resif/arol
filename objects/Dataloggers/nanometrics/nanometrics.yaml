mandatory_filters: [digitizer_manufacturer, digitizer_model, sampling_rate]

filters:
  - name: Digitizer manufacturer
    code: digitizer_manufacturer
    help: Select the manufacturer of your digitizer

  - name: Digitizer model
    code: digitizer_model
    help: Select the model of your digitizer

  - name: Version
    code: digitizer_version
    help: Select the version of digitizer you have

  - name: DC removal on/off
    code: dc_removal
    help: Is DC removal filter activated or not

  - name: Samples per seconds
    code: sampling_rate
    help: Select the sampling rate for this channel

  - name: Frontend gain
    code: preamplifier_gain
    help: Select the preamplifier gain (frontend)

responses:
  - path: nanometrics/CENTAUR-G-1.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      preamplifier_gain: Gain 1 (1x - 0dB - 40Vpp)
      digitizer_version: Standard gain

  - path: nanometrics/CENTAUR-G-2.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      preamplifier_gain: Gain 2 (2x - 0dB - 20Vpp)
      digitizer_version: Standard gain

  - path: nanometrics/CENTAUR-G-4.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      preamplifier_gain: Gain 4 (4x - 0dB - 10Vpp)

  - path: nanometrics/CENTAUR-G-10.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      preamplifier_gain: Gain 10 (10x - 0dB - 4Vpp)
      digitizer_version: Standard gain

  - path: nanometrics/CENTAUR-G-20.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      preamplifier_gain: Gain 20 (20x - 0dB - 2Vpp)
      digitizer_version: Standard gain

  - path: nanometrics/CENTAUR-G-40.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      preamplifier_gain: Gain 40 (40x - 0dB - 1Vpp)

  - path: nanometrics/CENTAUR-G-8.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      preamplifier_gain: Gain 8 (8x - 0dB - 5Vpp)
      digitizer_version: High gain

  - path: nanometrics/CENTAUR-G-16.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      preamplifier_gain: Gain 16 (16x - 0dB - 2.5Vpp)
      digitizer_version: High gain

  - path: nanometrics/CENTAUR.100.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      dc_removal: DC removal off
      sampling_rate: 100 sps

  - path: nanometrics/CENTAUR-DC.100.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      dc_removal: DC removal on
      sampling_rate: 100 sps

  - path: nanometrics/CENTAUR.200.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      dc_removal: DC removal off
      sampling_rate: 200 sps

  - path: nanometrics/CENTAUR.250.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Centaur (all versions)
      dc_removal: DC removal off
      sampling_rate: 250 sps

  - path: nanometrics/TAURUS-G-1.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model:  Taurus
      preamplifier_gain: Gain 1 (1x - 0dB - 16Vpp)

  - path: nanometrics/TAURUS-G-04.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model:  Taurus
      preamplifier_gain: Gain 0.4 (0.4x - 0dB - 40Vpp)

  - path: nanometrics/TAURUS-G-2.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model:  Taurus
      preamplifier_gain: Gain 2 (1x - 0dB - 8Vpp)

  - path: nanometrics/TAURUS.10.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Taurus
      sampling_rate: 10 sps

  - path: nanometrics/TAURUS.40.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Taurus
      sampling_rate: 40 sps

  - path: nanometrics/TAURUS.50.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Taurus
      sampling_rate: 50 sps

  - path: nanometrics/TAURUS.100.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Taurus
      sampling_rate: 100 sps

  - path: nanometrics/TAURUS.200.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Taurus
      sampling_rate: 200 sps

  - path: nanometrics/TAURUS.250.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Taurus
      sampling_rate: 250 sps

  - path: nanometrics/TRIDENT-G-1.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model:  Trident
      preamplifier_gain: Gain 1 (1x - 0dB - 16Vpp)

  - path: nanometrics/TRIDENT.100.response.yaml
    applicable_filters:
      digitizer_manufacturer: Nanometrics
      digitizer_model: Trident
      sampling_rate: 100 sps
